<?php

namespace Entity;

class User
{
    public function __construct(
        private int $id,
        private string $pseudo,
        private string $password,
        private string $description
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getPseudo(): string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;
        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function generateSlug(): string
    {
        $slug = str_replace(' ', '-', $this->getPseudo());
        $slug = strtolower($slug);
        $slug .= (string) $this->getId();

        return $slug;
    }
}