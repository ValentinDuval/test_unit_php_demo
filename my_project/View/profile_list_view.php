<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste des profils</title>
</head>
<body>
    <h1>Liste des utilisateurs</h1>
    <ul>
    <?php foreach ($users as $user): ?>
        <li>Profil de <a href="/profile_details.php?id=<?php echo $user->getId(); ?>"><?php echo $user->getPseudo(); ?></a></li>
    <?php endforeach; ?>
    </ul>
</body>
</html>
