<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profil de <?php echo $user->getPseudo(); ?></title>
</head>
<body>
    <h1>Profil de <?php echo $user->getPseudo(); ?></h1>
    <ul>
        <li>Id : <?php echo $user->getId(); ?></li>
        <li>Description : <?php echo $user->getDescription(); ?></li>
    </ul>
    <p><a href="/profile_view.php">Retour au menu</a></p>
</body>
</html>
