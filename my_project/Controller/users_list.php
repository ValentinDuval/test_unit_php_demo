<?php

require_once __DIR__ . '/../Model/Entity/User.php';

use Entity\User;

$users = [
    new User(1, 'toto', 'un mot de passe fort', 'Je découvre l\'informatique, soyez indulgents !'),
    new User(2, 'Lili02', 'f18r1#ff!SfrA', 'Mes passions : les échecs, le surf et les soirées entre amis !'),
    new User(3, 'Richard Dumont', 'password1234', 'Qui sait comment ajouter des amis ? Richard'),
];