<?php

if (!isset($_GET['id'])) {
    header('location:profile_view.php');
}

require_once 'users_list.php';
$selectedUser = null;

foreach ($users as $user) {
    if ((int) $_GET['id'] === $user->getId()) {
        $selectedUser = $user;
        break;
    }
}

if ($selectedUser === null) {
    header('location:profile_view.php');
}

require_once __DIR__ . '/../View/profile_details_view.php';